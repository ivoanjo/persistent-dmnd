source "https://rubygems.org"

gemspec

# Specify ffi version to avoid dependency issues when switching between MRI
# and JRuby with the same gems.lock
# Only used in development
gem "ffi", "~> 1.15.5"
