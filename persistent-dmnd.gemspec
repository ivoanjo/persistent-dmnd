# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "persistent_dmnd/version"

Gem::Specification.new do |spec|
  spec.name = "persistent-dmnd"
  spec.version = Persistent💎::VERSION
  spec.authors = "Ivo Anjo"
  spec.email = "ivo@ivoanjo.me"

  spec.summary = "Persistent-💎: Because Immutable Data Is Forever"
  spec.description = "A tiny ruby gem that gives you a beautiful short-hand syntax for creating immutable arrays, hashes and sets"
  spec.homepage = "https://gitlab.com/ivoanjo/persistent-dmnd/"
  spec.license = "MIT"

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ["lib"]

  spec.required_ruby_version = ">= 2.4.0"

  spec.add_development_dependency "bundler", "~> 1.17" if RUBY_VERSION < "2.6"
  spec.add_development_dependency "rake", "~> 13.1"
  spec.add_development_dependency "rspec", "~> 3.13"
  spec.add_development_dependency "concurrent-ruby", "~> 1.1"
  spec.add_development_dependency "standard" if RUBY_VERSION >= "2.6.0"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-byebug" if RUBY_ENGINE == "ruby"
  spec.add_development_dependency "pry-debugger-jruby" if RUBY_ENGINE == "jruby"
  spec.add_development_dependency "hamster", ">= 3.0.0"
  spec.add_development_dependency "warning", "~> 1.3.0"

  spec.add_dependency "immutable-ruby", ">= 0.1.0"
end
