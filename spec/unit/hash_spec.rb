# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent-💎"

require "hamster"

RSpec.describe Persistent💎::Hash do
  describe ".[]" do
    it "creates a new hash" do
      expect(described_class["hello" => "world"].class).to be described_class
    end

    it "creates a hash from the received arguments" do
      expect(described_class["hello" => "world"]).to eq("hello" => "world")
    end

    context "when no arguments are received" do
      it "returns an empty hash" do
        expect(described_class[]).to be_empty
      end

      it "reuses the empty hash instance" do
        expect(described_class[]).to be described_class[]
      end
    end
  end

  describe ".new" do
    it "creates a new hash" do
      expect(described_class.new("hello" => "world").class).to be described_class
    end

    it "creates a hash from the received arguments" do
      expect(described_class.new("hello" => "world")).to eq("hello" => "world")
    end

    context "when no arguments are received" do
      it "creates an empty hash" do
        expect(described_class.new).to be_empty
      end
    end
  end

  it do
    expect(described_class["hello" => "world"]).to be_persistent
  end

  it "can be compared with a Ruby hash" do
    expect({"hello" => "world"}).to eq described_class["hello" => "world"]
  end

  it "can be compared with a Immutable::Hash" do
    expect(Immutable::Hash.new("hello" => "world")).to eq described_class["hello" => "world"]
  end

  it "can compare itself with a Immutable::Hash" do
    expect(described_class["hello" => "world"]).to eq Immutable::Hash.new("hello" => "world")
  end

  it "can be compared with a Hamster::Hash" do
    expect(Hamster::Hash.new("hello" => "world")).to eq described_class["hello" => "world"]
  end

  it "can compare itself with a Hamster::Hash" do
    expect(described_class["hello" => "world"]).to eq Hamster::Hash.new("hello" => "world")
  end

  describe "#to_💎" do
    it "returns itself" do
      hash = described_class["hello" => "world"]

      expect(hash.to_💎).to be hash
    end

    # Make sure that JRubyWorkaround is correct
    context "when called with public_send" do
      it "returns itself" do
        hash = described_class["hello" => "world"]

        expect(hash.public_send(:to_💎)).to be hash
      end
    end
  end

  describe "#to_dmnd" do
    it "returns itself" do
      hash = described_class["hello" => "world"]

      expect(hash.to_dmnd).to be hash
    end
  end

  it do
    expect(described_class[]).to_not respond_to(:to_ary)
  end

  describe "#to_h" do
    it "returns a regular Ruby hash" do
      expect(described_class["hello" => "world"].to_h.class).to be ::Hash
    end

    it "returns a hash with the contents of the persistent hash" do
      expect(described_class["hello" => "world"].to_h).to eq("hello" => "world")
    end
  end

  describe "#to_hash" do
    it "returns a regular Ruby hash" do
      expect(described_class["hello" => "world"].to_hash.class).to be ::Hash
    end

    it "returns a hash with the contents of the persistent hash" do
      expect(described_class["hello" => "world"].to_hash).to eq("hello" => "world")
    end
  end

  describe "#to_set" do
    it "returns a regular Ruby set" do
      expect(described_class["hello" => "world"].to_set.class).to be ::Set
    end

    it "returns a set with the same elements as the persistent hash" do
      expect(described_class["hello" => "world"].to_set).to eq Set.new([["hello", "world"]])
    end
  end

  describe "#to_a💎" do
    include Persistent💎

    it "returns a Persistent💎::Array" do
      expect(described_class["hello" => "world"].to_a💎.class).to be Persistent💎::Array
    end

    it "returns an array of Persistent💎::Array pairs" do
      expect(described_class["hello" => "world"].to_a💎.map(&:class).uniq).to eq [Persistent💎::Array]
    end

    it "returns a persistent array of persistent array pairs with the contents of the hash" do
      expect(described_class["hello" => "world"].to_a💎).to eq a💎[a💎["hello", "world"]]
    end
  end

  describe "#to_aDmnd" do
    it "is the same as #to_a💎" do
      expect(described_class[].method(:to_aDmnd)).to eq described_class[].method(:to_a💎)
    end
  end

  describe "#to_h💎" do
    it "returns itself" do
      hash = described_class["hello" => "world"]

      expect(hash.to_h💎).to be hash
    end
  end

  describe "#to_hDmnd" do
    it "is the same as #to_h💎" do
      expect(described_class[].method(:to_hDmnd)).to eq described_class[].method(:to_h💎)
    end
  end

  describe "#to_s💎" do
    include Persistent💎

    it "returns a Persistent💎::Set" do
      expect(described_class["hello" => "world"].to_s💎.class).to be Persistent💎::Set
    end

    it "returns a set of Persistent💎::Array pairs" do
      expect(described_class["hello" => "world"].to_s💎.map(&:class)).to eq [Persistent💎::Array]
    end

    it "returns a set with the same elements as the persistent array" do
      expect(described_class["hello" => "world"].to_s💎).to eq s💎[a💎["hello", "world"]]
    end
  end

  describe "#to_sDmnd" do
    it "is the same as #to_s💎" do
      expect(described_class[].method(:to_sDmnd)).to eq described_class[].method(:to_s💎)
    end
  end

  describe "#each💎" do
    include Persistent💎

    context "when called with a block" do
      it "yields Persistent💎::Array elements" do
        result = []

        described_class["hello" => "world"].each💎 { |element| result << element }

        expect(result.map(&:class)).to eq [Persistent💎::Array]
      end

      it "yields key value pairs" do
        result = []

        described_class["hello" => "world"].each💎 { |element| result << element }

        expect(result).to eq [a💎["hello", "world"]]
      end
    end

    context "when called without a block" do
      it "returns an enumerator" do
        expect(described_class["hello" => "world"].each💎.class).to be Enumerator
      end

      it "returns an enumerator which yields Persistent💎::Array elements" do
        expect(described_class["hello" => "world"].each💎.to_a.map(&:class)).to eq [Persistent💎::Array]
      end

      it "returns an enumerator which yields key value pairs" do
        expect(described_class["hello" => "world"].each💎.to_a).to eq [a💎["hello", "world"]]
      end
    end
  end

  describe "#eachDmnd" do
    it "is the same as #each💎" do
      expect(described_class[].method(:eachDmnd)).to eq described_class[].method(:each💎)
    end
  end

  # see http://olivierlacan.com/posts/hash-comparison-in-ruby-2-3/
  # This is not exactly the same as set comparison, but it's easy to use
  # set nomenclature to distinguish the multiple cases
  shared_examples "Comparison between hashes" do |from, to|
    context "when #{from.fetch(:name)} is a subhash of the #{to.fetch(:name)}" do
      let(:subhash) { from.fetch(:subhash) }
      let(:superhash) { to.fetch(:superhash) }

      it "should be < than the #{to.fetch(:name)}" do
        expect(subhash).to be < superhash
      end

      it "should be <= than the #{to.fetch(:name)}" do
        expect(subhash).to be <= superhash
      end

      it "should not be >= than the #{to.fetch(:name)}" do
        expect(subhash).to_not be >= superhash
      end

      it "should not be > than the #{to.fetch(:name)}" do
        expect(subhash).to_not be > superhash
      end
    end

    context "when #{from.fetch(:name)} is a superhash of the #{to.fetch(:name)}" do
      let(:superhash) { from.fetch(:superhash) }
      let(:subhash) { to.fetch(:subhash) }

      it "should not be < than the #{to.fetch(:name)}" do
        expect(superhash).to_not be < subhash
      end

      it "should not be <= than the #{to.fetch(:name)}" do
        expect(superhash).to_not be <= subhash
      end

      it "should be >= than the #{to.fetch(:name)}" do
        expect(superhash).to be >= subhash
      end

      it "should be > than the #{to.fetch(:name)}" do
        expect(superhash).to be > subhash
      end
    end

    context "when comparing a #{from.fetch(:name)} with a #{to.fetch(:name)} with the same content" do
      let(:hash) { from.fetch(:subhash) }
      let(:another_hash) { to.fetch(:subhash) }

      it "should be <= than the #{to.fetch(:name)}" do
        expect(hash).to be <= another_hash
      end

      it "should be >= than the #{to.fetch(:name)}" do
        expect(hash).to be >= another_hash
      end

      it "should not be < than the #{to.fetch(:name)}" do
        expect(hash).to_not be < another_hash
      end

      it "should not be > than the #{to.fetch(:name)}" do
        expect(hash).to_not be > another_hash
      end
    end

    context "when comparing a #{from.fetch(:name)} with a disjoint #{to.fetch(:name)}" do
      let(:hash) { from.fetch(:subhash) }
      let(:disjoint) { to.fetch(:disjoint) }

      it "should not be < than the disjoint #{to.fetch(:name)}" do
        expect(hash).to_not be < disjoint
      end

      it "should not be <= than the disjoint #{to.fetch(:name)}" do
        expect(hash).to_not be <= disjoint
      end

      it "should not be > than the disjoint #{to.fetch(:name)}" do
        expect(hash).to_not be > disjoint
      end

      it "should not be >= than the disjoint #{to.fetch(:name)}" do
        expect(hash).to_not be >= disjoint
      end
    end
  end

  describe "Hash comparison" do
    options = [
      { # from
        name: "persistent hash",
        subhash: described_class["a" => 1],
        superhash: described_class["a" => 1, "b" => 2]
      },
      { # to
        name: "other persistent hash",
        subhash: described_class["a" => 1],
        superhash: described_class["a" => 1, "b" => 2],
        disjoint: described_class["a" => 3]
      }
    ]

    include_examples "Comparison between hashes", *options
  end

  describe "When comparing persistent hashes with Ruby hashes" do
    options = [
      { # from
        name: "persistent hash",
        subhash: described_class["a" => 1],
        superhash: described_class["a" => 1, "b" => 2]
      },
      { # to
        name: "Ruby hash",
        subhash: {"a" => 1},
        superhash: {"a" => 1, "b" => 2},
        disjoint: {"a" => 3}
      }
    ]

    include_examples "Comparison between hashes", *options
  end

  describe "When comparing Ruby hashes with persistent hashes" do
    options = [
      { # from
        name: "Ruby hash",
        subhash: {"a" => 1},
        superhash: {"a" => 1, "b" => 2}
      },
      { # to
        name: "persistent hash",
        subhash: described_class["a" => 1],
        superhash: described_class["a" => 1, "b" => 2],
        disjoint: described_class["a" => 3]
      }
    ]

    include_examples "Comparison between hashes", *options
  end

  describe "optional concurrent-ruby interoperability" do
    describe "#to_concurrent_hash" do
      before do
        Persistent💎::ConcurrentRubySupport.send(:reset_state)
      end

      it "returns a Concurrent::Hash" do
        expect(described_class["hello" => "world"].to_concurrent_hash.class).to be Concurrent::Hash
      end

      it "returns a hash with the contents of the persistent hash" do
        expect(described_class["hello" => "world"].to_concurrent_hash).to eq("hello" => "world")
      end

      it "ensures that concurrent-ruby is available before using it" do
        Persistent💎::ConcurrentRubySupport.send(:mark_as_failure)

        expect { described_class["hello" => "world"].to_concurrent_hash }.to raise_error(NotImplementedError)
      end
    end

    describe "#to_concurrent" do
      it "is the same as #to_concurrent_hash" do
        expect(described_class[].method(:to_concurrent)).to eq described_class[].method(:to_concurrent_hash)
      end
    end

    describe "#to_concurrent_map" do
      before do
        Persistent💎::ConcurrentRubySupport.send(:reset_state)
      end

      it "returns a Concurrent::Map" do
        expect(described_class["hello" => "world"].to_concurrent_map.class).to be Concurrent::Map
      end

      it "returns a map with the contents of the persistent hash" do
        map = described_class["hello" => "world"].to_concurrent_map

        map_to_hash = map.enum_for(:each_pair).to_h

        expect(map_to_hash).to eq("hello" => "world")
      end

      it "ensures that concurrent-ruby is available before using it" do
        Persistent💎::ConcurrentRubySupport.send(:mark_as_failure)

        expect { described_class["hello" => "world"].to_concurrent_map }.to raise_error(NotImplementedError)
      end
    end
  end
end
