# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent-💎"

RSpec.describe Persistent💎::ConcurrentRubySupport do
  describe ".ensure_concurrent_ruby_loaded" do
    before do
      reset_loaded_state
    end

    let(:ensure_concurrent_ruby_loaded) { described_class.ensure_concurrent_ruby_loaded }

    context "when concurrent-ruby is available" do
      context "when method has not been called before" do
        it do
          expect(described_class).to receive(:require).with("concurrent")

          ensure_concurrent_ruby_loaded
        end

        it do
          expect(ensure_concurrent_ruby_loaded).to be true
        end
      end

      context "when method has been called before" do
        before do
          described_class.ensure_concurrent_ruby_loaded
        end

        it do
          expect(described_class).to_not receive(:require).with("concurrent")

          ensure_concurrent_ruby_loaded
        end

        it do
          expect(ensure_concurrent_ruby_loaded).to be true
        end
      end
    end

    context "when concurrent-ruby is not available" do
      before do
        allow(described_class).to receive(:require).and_raise(LoadError)
      end

      context "when method has not been called before" do
        it do
          expect(described_class).to receive(:require).with("concurrent")

          begin
            ensure_concurrent_ruby_loaded
          rescue NotImplementedError
          end
        end

        it do
          expect { ensure_concurrent_ruby_loaded }.to raise_error(NotImplementedError)
        end
      end

      context "when method has been called before" do
        before do
          begin
            described_class.ensure_concurrent_ruby_loaded
          rescue NotImplementedError
          end
        end

        it do
          expect(described_class).to_not receive(:require).with("concurrent")

          begin
            ensure_concurrent_ruby_loaded
          rescue NotImplementedError
          end
        end

        it do
          expect { ensure_concurrent_ruby_loaded }.to raise_error(NotImplementedError)
        end
      end
    end

    def reset_loaded_state
      described_class.send(:reset_state)
    end
  end
end
