# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent-💎"

require "hamster"

RSpec.describe Persistent💎::Array do
  describe ".[]" do
    it "creates a new array" do
      expect(described_class[1, 2, 3].class).to be described_class
    end

    it "creates an array from the received arguments" do
      expect(described_class[1, 2, 3]).to eq [1, 2, 3]
    end

    context "when no arguments are received" do
      it "returns an empty array" do
        expect(described_class[]).to be_empty
      end

      it "reuses the empty array instance" do
        expect(described_class[]).to be described_class[]
      end
    end
  end

  describe ".new" do
    it "creates a new array" do
      expect(described_class.new([1, 2, 3]).class).to be described_class
    end

    it "creates an array from the received arguments" do
      expect(described_class.new([1, 2, 3])).to eq [1, 2, 3]
    end

    context "when no arguments are received" do
      it "creates an empty array" do
        expect(described_class.new).to be_empty
      end
    end
  end

  it do
    expect(described_class[1, 2, 3]).to be_persistent
  end

  it "can be compared with a Ruby array" do
    expect([:hello, :world]).to eq described_class[:hello, :world]
  end

  it "can be compared with a Immutable::Vector" do
    expect(Immutable::Vector.new([:hello, :world])).to eq described_class[:hello, :world]
  end

  it "can compare itself with a Immutable::Vector" do
    expect(described_class[:hello, :world]).to eq Immutable::Vector.new([:hello, :world])
  end

  it "can be compared with a Hamster::Vector" do
    expect(Hamster::Vector.new([:hello, :world])).to eq described_class[:hello, :world]
  end

  it "can compare itself with a Hamster::Vector" do
    expect(described_class[:hello, :world]).to eq Hamster::Vector.new([:hello, :world])
  end

  describe "#to_💎" do
    it "returns itself" do
      array = described_class[1, 2, 3]

      expect(array.to_💎).to be array
    end
  end

  describe "#to_dmnd" do
    it "returns itself" do
      array = described_class[1, 2, 3]

      expect(array.to_dmnd).to be array
    end
  end

  describe "#to_a" do
    it "returns a regular Ruby array" do
      expect(described_class[1, 2, 3].to_a.class).to be ::Array
    end

    it "returns an array with the contents of the persistent array" do
      expect(described_class[1, 2, 3].to_a).to eq [1, 2, 3]
    end
  end

  describe "#to_ary" do
    it "returns a regular Ruby array" do
      expect(described_class[1, 2, 3].to_ary.class).to be ::Array
    end

    it "returns an array with the contents of the persistent array" do
      expect(described_class[1, 2, 3].to_ary).to eq [1, 2, 3]
    end
  end

  describe "#to_set" do
    it "returns a regular Ruby set" do
      expect(described_class[1, 2, 3].to_set.class).to be ::Set
    end

    it "returns a set with the same elements as the persistent array" do
      expect(described_class[1, 2, 3].to_set).to eq Set.new([1, 2, 3])
    end
  end

  describe "#to_a💎" do
    it "returns itself" do
      array = described_class[1, 2, 3]

      expect(array.to_a💎).to be array
    end
  end

  describe "#to_aDmnd" do
    it "is the same as #to_a💎" do
      expect(described_class[].method(:to_aDmnd)).to eq described_class[].method(:to_a💎)
    end
  end

  describe "#to_h💎" do
    include Persistent💎

    it "returns a Persistent💎::Hash" do
      expect(described_class[described_class[:hello, :world]].to_h💎.class).to be Persistent💎::Hash
    end

    it "returns a hash created from pairs inside the persistent array" do
      expect(described_class[described_class[:hello, :world]].to_h💎).to eq h💎[hello: :world]
    end
  end

  describe "#to_hDmnd" do
    it "is the same as #to_h💎" do
      expect(described_class[].method(:to_hDmnd)).to eq described_class[].method(:to_h💎)
    end
  end

  describe "#to_s💎" do
    include Persistent💎

    it "returns a Persistent💎::Set" do
      expect(described_class[1, 2, 3].to_s💎.class).to be Persistent💎::Set
    end

    it "returns a set with the same elements as the persistent array" do
      expect(described_class[1, 2, 3].to_s💎).to eq s💎[1, 2, 3]
    end
  end

  describe "#to_sDmnd" do
    it "is the same as #to_s💎" do
      expect(described_class[].method(:to_sDmnd)).to eq described_class[].method(:to_s💎)
    end
  end

  describe "optional concurrent-ruby interoperability" do
    describe "#to_concurrent_array" do
      before do
        Persistent💎::ConcurrentRubySupport.send(:reset_state)
      end

      it "returns a Concurrent::Array" do
        expect(described_class[1, 2, 3].to_concurrent_array.class).to be Concurrent::Array
      end

      it "returns an array with the contents of the persistent array" do
        expect(described_class[1, 2, 3].to_concurrent_array).to eq [1, 2, 3]
      end

      it "ensures that concurrent-ruby is available before using it" do
        Persistent💎::ConcurrentRubySupport.send(:mark_as_failure)

        expect { described_class[1, 2, 3].to_concurrent_array }.to raise_error(NotImplementedError)
      end
    end

    describe "#to_concurrent" do
      it "is the same as #to_concurrent_array" do
        expect(described_class[].method(:to_concurrent)).to eq described_class[].method(:to_concurrent_array)
      end
    end

    describe "#to_concurrent_tuple" do
      before do
        Persistent💎::ConcurrentRubySupport.send(:reset_state)
      end

      it "returns a Concurrent::Tuple" do
        expect(described_class[1, 2, 3].to_concurrent_tuple.class).to be Concurrent::Tuple
      end

      it "returns a tuple with the contents of the persistent array" do
        expect(described_class[1, 2, 3].to_concurrent_tuple.to_a).to eq [1, 2, 3]
      end

      it "ensures that concurrent-ruby is available before using it" do
        Persistent💎::ConcurrentRubySupport.send(:mark_as_failure)

        expect { described_class[1, 2, 3].to_concurrent_tuple }.to raise_error(NotImplementedError)
      end
    end
  end
end
