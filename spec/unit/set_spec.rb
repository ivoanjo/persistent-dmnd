# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent-💎"

require "set"
require "hamster"

RSpec.describe Persistent💎::Set do
  describe ".[]" do
    it "creates a new set" do
      expect(described_class[:hello, :world].class).to be described_class
    end

    it "creates a set from the received arguments" do
      expect(described_class[:hello, :world]).to eq Set.new([:hello, :world])
    end

    context "when no arguments are received" do
      it "returns an empty set" do
        expect(described_class[]).to be_empty
      end

      it "reuses the empty set instance" do
        expect(described_class[]).to be described_class[]
      end
    end
  end

  describe ".new" do
    it "creates a new set" do
      expect(described_class.new([:hello, :world]).class).to be described_class
    end

    it "creates a set from the received arguments" do
      expect(described_class.new([:hello, :world])).to eq Set.new([:hello, :world])
    end

    context "when no arguments are received" do
      it "creates an empty set" do
        expect(described_class.new).to be_empty
      end
    end
  end

  describe ".from_set" do
    context "when a regular Ruby set is passed in" do
      it "creates a new set from the regular Ruby set" do
        expect(described_class.from_set(Set.new([:hello, :world])).class).to be described_class
      end

      it "creates a set with the same elements of the set" do
        expect(described_class.from_set(Set.new([:hello, :world]))).to eq Set.new([:hello, :world])
      end

      context "when set is empty" do
        it "reuses the empty set instance" do
          expect(described_class.from_set(Set.new)).to be described_class[]
        end
      end
    end
  end

  it do
    expect(described_class[:hello, :world]).to be_persistent
  end

  it "can be compared with a Ruby set" do
    expect(Set.new([:hello, :world])).to eq described_class[:hello, :world]
  end

  it "can be flattened when inside a Ruby set" do
    expect(Set.new([described_class[0, described_class[1]]]).flatten).to eq(Set.new([0, 1]))
  end

  it "can be in-place flattened when inside a Ruby set" do
    skip "Unfortunately this cannot be implemented without monkey patching the " \
            "current flatten! implementation on Set."

    result = Set.new([described_class[0, described_class[1]]])
    result.flatten!

    expect(result).to eq(Set.new([0, 1]))
  end

  it "can be tested with intersect? against a Ruby set" do
    expect(Set.new([0]).intersect?(described_class[0])).to be true
  end

  it "can be tested with disjoint? against a Ruby set" do
    expect(Set.new([0]).disjoint?(described_class[1])).to be true
  end

  it "can be compared with a Ruby array" do
    expect([:hello, :world]).to eq described_class[:world, :hello]
  end

  it "can compare itself with something that answers to #to_set" do
    dummy = double("Dummy", to_set: Set.new([:hello, :world]))

    expect(described_class[:hello, :world]).to eq dummy
  end

  it "can compare itself with a Immutable::SortedSet" do
    expect(described_class[:hello, :world]).to eq Immutable::SortedSet.new([:hello, :world])
  end

  it "can be compared with a Immutable::Set" do
    pending "Unfortunately this cannot be implemented without monkey patching the " \
            "current eql? implementation on Immutable::Set or upstream changes."

    expect(Immutable::Set.new([:hello, :world])).to eq described_class[:hello, :world]
  end

  it "can compare itself with a Immutable::Set" do
    expect(described_class[:hello, :world]).to eq Immutable::Set.new([:hello, :world])
  end

  it "can compare itself with a Hamster::SortedSet" do
    expect(described_class[:hello, :world]).to eq Hamster::SortedSet.new([:hello, :world])
  end

  it "can be compared with a Hamster::Set" do
    pending "Unfortunately this cannot be implemented without monkey patching the " \
            "current eql? implementation on Hamster::Set or upstream changes."

    expect(Hamster::Set.new([:hello, :world])).to eq described_class[:hello, :world]
  end

  it "can compare itself with a Hamster::Set" do
    expect(described_class[:hello, :world]).to eq Hamster::Set.new([:hello, :world])
  end

  it "is ordered" do
    expect(described_class[:c, :a, :b]).to eq [:a, :b, :c]
  end

  describe "#to_a" do
    it "returns a regular Ruby array" do
      expect(described_class[1, 2, 3].to_a.class).to be ::Array
    end

    it "returns an array with the contents of the persistent set" do
      expect(described_class[1, 2, 3].to_a.sort).to eq [1, 2, 3].sort
    end
  end

  describe "#to_set" do
    it "returns a regular Ruby set" do
      expect(described_class[:hello, :world].to_set.class).to be ::Set
    end

    it "returns a set with the same elements as the persistent set" do
      expect(described_class[:hello, :world].to_set).to eq Set.new([:hello, :world])
    end
  end

  describe "#to_💎" do
    it "returns itself" do
      set = described_class[:hello, :world]

      expect(set.to_💎).to be set
    end
  end

  describe "#to_dmnd" do
    it "returns itself" do
      set = described_class[:hello, :world]

      expect(set.to_dmnd).to be set
    end
  end

  describe "#to_a💎" do
    include Persistent💎

    it "returns a Persistent💎::Array" do
      expect(described_class[1, 2, 3].to_a💎.class).to be Persistent💎::Array
    end

    it "returns an array with the same elements as the persistent array" do
      expect(described_class[1, 2, 3].to_a💎.sort).to eq a💎[1, 2, 3]
    end
  end

  describe "#to_aDmnd" do
    it "is the same as #to_a💎" do
      expect(described_class[].method(:to_aDmnd)).to eq described_class[].method(:to_a💎)
    end
  end

  describe "#to_h💎" do
    include Persistent💎

    it "returns a Persistent💎::Hash" do
      expect(described_class[[:hello, :world]].to_h💎.class).to be Persistent💎::Hash
    end

    it "returns a hash created from pairs inside the persistent array" do
      expect(described_class[[:hello, :world]].to_h💎).to eq h💎[hello: :world]
    end
  end

  describe "#to_hDmnd" do
    it "is the same as #to_h💎" do
      expect(described_class[].method(:to_hDmnd)).to eq described_class[].method(:to_h💎)
    end
  end

  describe "#to_s💎" do
    it "returns itself" do
      set = described_class[1, 2, 3]

      expect(set.to_s💎).to be set
    end
  end

  describe "#to_sDmnd" do
    it "is the same as #to_s💎" do
      expect(described_class[].method(:to_sDmnd)).to eq described_class[].method(:to_s💎)
    end
  end

  describe "#===" do
    it "is an alias for include?" do
      expect(described_class.instance_method(:===)).to eq described_class.instance_method(:include?)
    end
  end

  shared_examples "Comparison between sets" do |from, to|
    context "when #{from.fetch(:name)} is a subset of the #{to.fetch(:name)}" do
      let(:subset) { from.fetch(:subset) }
      let(:superset) { to.fetch(:superset) }

      it "should be < than the #{to.fetch(:name)}" do
        expect(subset).to be < superset
      end

      it "should be <= than the #{to.fetch(:name)}" do
        expect(subset).to be <= superset
      end

      it "should not be >= than the #{to.fetch(:name)}" do
        expect(subset).to_not be >= superset
      end

      it "should not be > than the #{to.fetch(:name)}" do
        expect(subset).to_not be > superset
      end
    end

    context "when #{from.fetch(:name)} is a superset of the #{to.fetch(:name)}" do
      let(:superset) { from.fetch(:superset) }
      let(:subset) { to.fetch(:subset) }

      it "should not be < than the #{to.fetch(:name)}" do
        expect(superset).to_not be < subset
      end

      it "should not be <= than the #{to.fetch(:name)}" do
        expect(superset).to_not be <= subset
      end

      it "should be >= than the #{to.fetch(:name)}" do
        expect(superset).to be >= subset
      end

      it "should be > than the #{to.fetch(:name)}" do
        expect(superset).to be > subset
      end
    end

    context "when comparing a #{from.fetch(:name)} with a #{to.fetch(:name)} with the same content" do
      let(:set) { from.fetch(:subset) }
      let(:another_set) { to.fetch(:subset) }

      it "should be <= than the #{to.fetch(:name)}" do
        expect(set).to be <= another_set
      end

      it "should be >= than the #{to.fetch(:name)}" do
        expect(set).to be >= another_set
      end

      it "should not be < than the #{to.fetch(:name)}" do
        expect(set).to_not be < another_set
      end

      it "should not be > than the #{to.fetch(:name)}" do
        expect(set).to_not be > another_set
      end
    end

    context "when comparing a #{from.fetch(:name)} with a disjoint #{to.fetch(:name)}" do
      let(:set) { from.fetch(:subset) }
      let(:disjoint) { to.fetch(:disjoint) }

      it "should not be < than the disjoint #{to.fetch(:name)}" do
        expect(set).to_not be < disjoint
      end

      it "should not be <= than the disjoint #{to.fetch(:name)}" do
        expect(set).to_not be <= disjoint
      end

      it "should not be > than the disjoint #{to.fetch(:name)}" do
        expect(set).to_not be > disjoint
      end

      it "should not be >= than the disjoint #{to.fetch(:name)}" do
        expect(set).to_not be >= disjoint
      end
    end
  end

  shared_examples "Comparison between sets with spaceship operator (equal, disjoint)" do |from, to|
    describe "#<=>" do
      context "when comparing a #{from.fetch(:name)} with an equal #{to.fetch(:name)}" do
        let(:set) { from.fetch(:subset) }
        let(:another_set) { to.fetch(:subset) }

        it do
          expect(set <=> another_set).to be 0
        end
      end

      context "when comparing a #{from.fetch(:name)} with a disjoint #{to.fetch(:name)}" do
        let(:set) { from.fetch(:subset) }
        let(:disjoint) { to.fetch(:disjoint) }

        it do
          expect(set <=> disjoint).to be nil
        end
      end
    end
  end

  # Note: Behavior changed from Ruby < 3.0 (returned nil) to match Ruby 3.0,
  #       see also https://rubyreferences.github.io/rubychanges/3.0.html#standard-library
  shared_examples "Comparison between sets with spaceship operator (subset, superset)" do |from, to|
    describe "#<=>" do
      context "when comparing a #{from.fetch(:name)} subset with a #{to.fetch(:name)} superset" do
        let(:subset) { from.fetch(:subset) }
        let(:superset) { to.fetch(:superset) }

        it do
          expect(subset <=> superset).to be(-1)
        end
      end

      context "when comparing a #{from.fetch(:name)} superset with a #{to.fetch(:name)} subset" do
        let(:superset) { from.fetch(:superset) }
        let(:subset) { to.fetch(:subset) }

        it do
          expect(superset <=> subset).to be(+1)
        end
      end
    end
  end

  describe "Set comparison" do
    options = [
      { # from
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2]
      },
      { # to
        name: "other persistent set",
        subset: described_class[1],
        superset: described_class[1, 2],
        disjoint: described_class[3]
      }
    ]

    include_examples "Comparison between sets", *options
    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options
    include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
  end

  describe "When comparing persistent sets with Ruby sets" do
    options = [
      { # from
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2]
      },
      { # to
        name: "Ruby set",
        subset: Set[1],
        superset: Set[1, 2],
        disjoint: Set[3]
      }
    ]

    include_examples "Comparison between sets", *options
    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options
    include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
  end

  describe "When comparing Ruby sets with persistent sets" do
    options = [
      { # from
        name: "Ruby set",
        subset: Set[1],
        superset: Set[1, 2]
      },
      { # to
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2],
        disjoint: described_class[3]
      }
    ]

    context do
      include_examples "Comparison between sets", *options
    end

    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options

    context do
      before do
        skip "Set did not support subset/superset comparison using <=> before 3.0" if RUBY_VERSION < "3."
      end

      include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
    end
  end

  describe "When comparing persistent sets with Immutable::Set sets" do
    options = [
      { # from
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2]
      },
      { # to
        name: "Immutable::Set set",
        subset: Immutable::Set[1],
        superset: Immutable::Set[1, 2],
        disjoint: Immutable::Set[3]
      }
    ]

    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options
    include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
  end

  describe "When comparing persistent sets with Hamster::Set sets" do
    options = [
      { # from
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2]
      },
      { # to
        name: "Hamster::Set set",
        subset: Hamster::Set[1],
        superset: Hamster::Set[1, 2],
        disjoint: Hamster::Set[3]
      }
    ]

    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options
    include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
  end

  describe "When comparing Immutable::Set sets with persistent sets" do
    options = [
      { # from
        name: "Immutable::Set set",
        subset: Immutable::Set[1],
        superset: Immutable::Set[1, 2]
      },
      { # to
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2],
        disjoint: described_class[3]
      }
    ]

    include_examples "Comparison between sets", *options

    it "Immutable::Set does not have <=>" do
      expect(Immutable::Set[]).to_not respond_to(:<=>)
    end
  end

  describe "When comparing Hamster::Set sets with persistent sets" do
    options = [
      { # from
        name: "Hamster::Set set",
        subset: Hamster::Set[1],
        superset: Hamster::Set[1, 2]
      },
      { # to
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2],
        disjoint: described_class[3]
      }
    ]

    include_examples "Comparison between sets", *options

    it "Hamster::Set does not have <=>" do
      expect(Hamster::Set[]).to_not respond_to(:<=>)
    end
  end

  describe "When comparing persistent sets with Immutable::SortedSet sets" do
    options = [
      { # from
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2]
      },
      { # to
        name: "Immutable::SortedSet set",
        subset: Immutable::SortedSet[1],
        superset: Immutable::SortedSet[1, 2],
        disjoint: Immutable::SortedSet[3]
      }
    ]

    include_examples "Comparison between sets", *options
    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options
    include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
  end

  describe "When comparing persistent sets with Hamster::SortedSet sets" do
    options = [
      { # from
        name: "persistent set",
        subset: described_class[1],
        superset: described_class[1, 2]
      },
      { # to
        name: "Hamster::SortedSet set",
        subset: Hamster::SortedSet[1],
        superset: Hamster::SortedSet[1, 2],
        disjoint: Hamster::SortedSet[3]
      }
    ]

    include_examples "Comparison between sets", *options
    include_examples "Comparison between sets with spaceship operator (equal, disjoint)", *options
    include_examples "Comparison between sets with spaceship operator (subset, superset)", *options
  end
end
