# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent-💎"

require "concurrent"
require "hamster"

RSpec.describe Persistent💎::Dmndifier do
  include Persistent💎

  describe ".[]" do
    context "when argument is a Ruby array" do
      it "returns a persistent array with the contents of the array" do
        result = described_class[[:hello]]

        expect(result).to eq [:hello]
        expect(result.class).to be Persistent💎::Array
      end

      context "when Ruby array is empty" do
        it "reuses the empty array instance" do
          result = described_class[[]]

          expect(result).to be a💎[]
        end
      end
    end

    context "when argument is an Immutable::Vector" do
      it "returns a persistent array with the contents of the Immutable::Vector" do
        result = described_class[Immutable::Vector[:hello]]

        expect(result).to eq [:hello]
        expect(result.class).to be Persistent💎::Array
      end

      context "when Immutable::Vector is empty" do
        it "reuses the empty array instance" do
          result = described_class[Immutable::Vector[]]

          expect(result).to be a💎[]
        end
      end
    end

    context "when argument is a Hamster::Vector" do
      it "returns a persistent array with the contents of the Hamster::Vector" do
        result = described_class[Hamster::Vector[:hello]]

        expect(result).to eq [:hello]
        expect(result.class).to be Persistent💎::Array
      end

      context "when Hamster::Vector is empty" do
        it "reuses the empty array instance" do
          result = described_class[Hamster::Vector[]]

          expect(result).to be a💎[]
        end
      end
    end

    context "when argument is a Concurrent::Array" do
      it "returns a persistent array with the contents of the Concurrent::Array" do
        result = described_class[Concurrent::Array[:hello]]

        expect(result).to eq [:hello]
        expect(result.class).to be Persistent💎::Array
      end

      context "when Concurrent::Array is empty" do
        it "reuses the empty array instance" do
          result = described_class[Concurrent::Array[]]

          expect(result).to be a💎[]
        end
      end
    end

    context "when argument is a Concurrent::Tuple" do
      it "returns a persistent array with the contents of the Concurrent::Tuple" do
        tuple = Concurrent::Tuple.new(1)
        tuple.set(0, :hello)

        result = described_class[tuple]

        expect(result).to eq [:hello]
        expect(result.class).to be Persistent💎::Array
      end

      context "when Concurrent::Tuple.new is empty" do
        it "reuses the empty array instance" do
          result = described_class[Concurrent::Tuple.new(0)]

          expect(result).to be a💎[]
        end
      end
    end

    context "when argument is a Ruby hash" do
      it "returns a persistent hash with the contents of the hash" do
        result = described_class[{"hello" => "world"}]

        expect(result).to eq("hello" => "world")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when Ruby hash is empty" do
        it "reuses the empty hash instance" do
          result = described_class[{}]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument is an Immutable::Hash" do
      it "returns a persistent hash with the contents of the Immutable::Hash" do
        result = described_class[Immutable::Hash["hello" => "world"]]

        expect(result).to eq("hello" => "world")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when Immutable::Hash is empty" do
        it "reuses the empty hash instance" do
          result = described_class[Immutable::Hash[]]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument is a Hamster::Hash" do
      it "returns a persistent hash with the contents of the Hamster::Hash" do
        result = described_class[Hamster::Hash["hello" => "world"]]

        expect(result).to eq("hello" => "world")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when Hamster::Hash is empty" do
        it "reuses the empty hash instance" do
          result = described_class[Hamster::Hash[]]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument is a Concurrent::Hash" do
      it "returns a persistent hash with the contents of the Concurrent::Hash" do
        result = described_class[Concurrent::Hash["hello" => "world"]]

        expect(result).to eq("hello" => "world")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when Concurrent::Hash is empty" do
        it "reuses the empty hash instance" do
          result = described_class[Concurrent::Hash[]]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument is a Concurrent::Map" do
      it "returns a persistent hash with the contents of the Concurrent::Map" do
        map = Concurrent::Map.new
        map["hello"] = "world"

        result = described_class[map]

        expect(result).to eq("hello" => "world")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when Concurrent::Map is empty" do
        it "reuses the empty hash instance" do
          result = described_class[Concurrent::Map.new]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument is a Ruby set" do
      it "returns a persistent set with the contents of the set" do
        result = described_class[Set.new([:hello])]

        expect(result).to eq Set.new([:hello])
        expect(result.class).to be Persistent💎::Set
      end

      context "when Ruby set is empty" do
        it "reuses the empty set instance" do
          result = described_class[Set.new]

          expect(result).to be s💎[]
        end
      end
    end

    context "when argument is an Immutable::Set" do
      it "returns a persistent set with the contents of the Immutable::Set" do
        result = described_class[Immutable::Set[:hello]]

        expect(result).to eq Set.new([:hello])
        expect(result.class).to be Persistent💎::Set
      end

      context "when Immutable::Set is empty" do
        it "reuses the empty set instance" do
          result = described_class[Immutable::Set[]]

          expect(result).to be s💎[]
        end
      end
    end

    context "when argument is an Immutable::SortedSet" do
      it "returns a persistent set with the contents of the Immutable::SortedSet" do
        result = described_class[Immutable::SortedSet[:hello]]

        expect(result).to eq Set.new([:hello])
        expect(result.class).to be Persistent💎::Set
      end

      context "when Immutable::SortedSet is empty" do
        it "reuses the empty set instance" do
          result = described_class[Immutable::SortedSet[]]

          expect(result).to be s💎[]
        end
      end
    end

    context "when argument is a Hamster::Set" do
      it "returns a persistent set with the contents of the Hamster::Set" do
        result = described_class[Hamster::Set[:hello]]

        expect(result).to eq Set.new([:hello])
        expect(result.class).to be Persistent💎::Set
      end

      context "when Hamster::Set is empty" do
        it "reuses the empty set instance" do
          result = described_class[Hamster::Set[]]

          expect(result).to be s💎[]
        end
      end
    end

    context "when argument is a Hamster::SortedSet" do
      it "returns a persistent set with the contents of the Hamster::SortedSet" do
        result = described_class[Hamster::SortedSet[:hello]]

        expect(result).to eq Set.new([:hello])
        expect(result.class).to be Persistent💎::Set
      end

      context "when Hamster::SortedSet is empty" do
        it "reuses the empty set instance" do
          result = described_class[Hamster::SortedSet[]]

          expect(result).to be s💎[]
        end
      end
    end

    context "when argument is nil" do
      it do
        expect { described_class[nil] }.to raise_error(ArgumentError)
      end
    end

    context "when argument responds to #to_💎" do
      it "returns the result of calling #to_💎 on the object" do
        dummy = double("Dummy")
        def dummy.to_💎
          :converted_dummy
        end

        expect(described_class[dummy]).to be :converted_dummy
      end
    end

    context "when argument responds to #to_dmnd" do
      it "returns the result of calling #to_dmnd on the object" do
        dummy = double("Dummy", to_dmnd: :converted_dummy)

        expect(described_class[dummy]).to be :converted_dummy
      end
    end

    context "when argument responds to #to_ary" do
      it "creates a persistent array with the result of #to_ary" do
        dummy = double("Dummy", to_ary: [:converted_dummy])

        result = described_class[dummy]

        expect(result).to eq [:converted_dummy]
        expect(result.class).to be Persistent💎::Array
      end

      context "when result of #to_ary is empty" do
        it "reuses the empty array instance" do
          dummy = double("Dummy", to_ary: [])

          result = described_class[dummy]

          expect(result).to be a💎[]
        end
      end
    end

    context "when argument responds to #to_hash" do
      it "creates a persistent hash with the result of #to_hash" do
        dummy = double("Dummy", to_hash: {"converted" => "dummy"})

        result = described_class[dummy]

        expect(result).to eq("converted" => "dummy")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when result of #to_hash is empty" do
        it "reuses the empty hash instance" do
          dummy = double("Dummy", to_hash: {})

          result = described_class[dummy]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument responds to #to_set" do
      it "creates a persistent set with the result of #to_set" do
        dummy = double("Dummy", to_set: Set.new([:converted_dummy]))

        result = described_class[dummy]

        expect(result).to eq Set.new([:converted_dummy])
        expect(result.class).to be Persistent💎::Set
      end

      context "when result of #to_set is empty" do
        it "reuses the empty set instance" do
          dummy = double("Dummy", to_set: Set.new)

          result = described_class[dummy]

          expect(result).to be s💎[]
        end
      end
    end

    context "when argument responds to #each_pair" do
      it "creates a persistent hash with the result of #each_pair.to_h" do
        dummy = double("Dummy", each_pair: {"converted" => "dummy"}.each_pair)

        result = described_class[dummy]

        expect(result).to eq("converted" => "dummy")
        expect(result.class).to be Persistent💎::Hash
      end

      context "when result of #each_pair.to_h is empty" do
        it "reuses the empty hash instance" do
          dummy = double("Dummy", each_pair: {}.each_pair)

          result = described_class[dummy]

          expect(result).to be h💎[]
        end
      end
    end

    context "when argument cannot be converted to something persistent" do
      let(:dummy) { double("Dummy") }

      it do
        expect { described_class[dummy] }.to raise_error(ArgumentError)
      end

      context "when an on_unknown object is specified" do
        it "calls the on_unknown object with the value that cannot be converted" do
          on_unknown = double("On Unknown", call: nil)

          expect(on_unknown).to receive(:call).with(dummy)

          described_class[dummy, on_unknown: on_unknown]
        end

        it "returns the result of calling the on_unknown object" do
          on_unknown = proc { :on_unknown_result }

          expect(described_class[dummy, on_unknown: on_unknown]).to be :on_unknown_result
        end
      end

      context "when an empty options hash is specified" do
        it "still uses the default on_unknown behavior" do
          expect { described_class[dummy, {}] }.to raise_error(ArgumentError)
        end
      end
    end
  end
end
