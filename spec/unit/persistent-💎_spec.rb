# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent-💎"

RSpec.describe Persistent💎 do
  it "has a version number" do
    expect(described_class::VERSION).not_to be nil
  end

  context "when including the Persistent💎 module" do
    include described_class

    describe "creation methods" do
      describe "#a💎" do
        it "returns the Array class" do
          expect(a💎).to be Persistent💎::Array
        end
      end

      describe "#aDmnd" do
        it "returns the Array class" do
          expect(aDmnd).to be Persistent💎::Array
        end
      end

      describe "#h💎" do
        it "returns the Hash class" do
          expect(h💎).to be Persistent💎::Hash
        end
      end

      describe "#hDmnd" do
        it "returns the Hash class" do
          expect(hDmnd).to be Persistent💎::Hash
        end
      end

      describe "#s💎" do
        it "returns the Set class" do
          expect(s💎).to be Persistent💎::Set
        end
      end

      describe "#sDmnd" do
        it "returns the Set class" do
          expect(sDmnd).to be Persistent💎::Set
        end
      end
    end

    it "adds the module to the including class' singleton class" do
      expect(self.class.singleton_class.included_modules).to include(described_class)
    end

    describe "#💎ify" do
      it "returns the Dmndifier class" do
        expect(💎ify).to be Persistent💎::Dmndifier
      end
    end

    describe "#dmndify" do
      it "returns the Dmndifier class" do
        expect(💎ify).to be Persistent💎::Dmndifier
      end
    end
  end

  # Not very useful, but more of a "why not?" thing
  context "when prepending the Persistent💎 module" do
    prepend described_class

    it "adds the module to the including class' singleton class" do
      expect(self.class.singleton_class.included_modules).to include(described_class)
    end
  end
end
