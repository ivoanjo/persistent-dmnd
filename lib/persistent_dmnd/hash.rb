# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent_dmnd/self_conversion"
require "persistent_dmnd/is_persistent"
require "persistent_dmnd/concurrent_ruby_support"

require "immutable"
require "set"

module Persistent💎
  class Hash < Immutable::Hash
    include SelfConversion
    include IsPersistent
    include Persistent💎

    # Hashes are not arrays and thus should not implement :to_ary
    undef_method :to_ary

    # Return Concurrent::Hash with contents of Persistent💎::Hash
    #
    # @example
    #   my_hash = h💎[hello: :world]
    #   my_concurrent_hash = my_hash.to_concurrent_hash
    #
    def to_concurrent_hash
      ConcurrentRubySupport.ensure_concurrent_ruby_loaded
      Concurrent::Hash[self]
    end

    alias_method :to_concurrent, :to_concurrent_hash

    # Return Concurrent::Map with contents of Persistent💎::Hash
    #
    # @example
    #   my_hash = h💎[hello: :world]
    #   my_concurrent_map = my_hash.to_concurrent_map
    #   # => #<Concurrent::Map:0x0055ad9b283ea0 entries=1 default_proc=nil>
    #
    def to_concurrent_map
      ConcurrentRubySupport.ensure_concurrent_ruby_loaded
      each_with_object(Concurrent::Map.new(initial_capacity: size)) do |(key, value), result|
        result[key] = value
      end
    end

    def to_set
      ::Set.new(self)
    end

    # Return each entry as a key, value pair inside an immutable array
    def each💎
      if block_given?
        each { |pair| yield a💎[*pair] }
      else
        enum_for(:each💎)
      end
    end

    alias_method :eachDmnd, :each💎

    def to_a💎
      a💎[*each💎]
    end

    alias_method :to_aDmnd, :to_a💎

    def to_h💎
      self
    end

    alias_method :to_hDmnd, :to_h💎

    def to_s💎
      s💎[*each💎]
    end

    alias_method :to_sDmnd, :to_s💎
  end
end
