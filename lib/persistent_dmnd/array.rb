# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent_dmnd/self_conversion"
require "persistent_dmnd/is_persistent"
require "persistent_dmnd/concurrent_ruby_support"

require "immutable"
require "set"

module Persistent💎
  class Array < Immutable::Vector
    include SelfConversion
    include IsPersistent
    include Persistent💎

    def self.[](*items)
      if items.empty?
        empty
      else
        super
      end
    end

    # Return Concurrent::Array with contents of Persistent💎::Array
    #
    # @example
    #   my_array = a💎[:hello, :world]
    #   my_concurrent_array = my_array.to_concurrent_array
    #
    def to_concurrent_array
      ConcurrentRubySupport.ensure_concurrent_ruby_loaded
      Concurrent::Array.new(self)
    end

    alias_method :to_concurrent, :to_concurrent_array

    # Return Concurrent::Tuple with contents of Persistent💎::Array
    #
    # @example
    #   my_array = a💎[:hello, :world]
    #   my_concurrent_tuple = my_array.to_concurrent_tuple
    #   # => #<Concurrent::Tuple @size=2, @tuple=[<#Concurrent::AtomicReference value:hello>, <#Concurrent::AtomicReference value:world>]>
    #
    def to_concurrent_tuple
      ConcurrentRubySupport.ensure_concurrent_ruby_loaded
      each.with_index.each_with_object(Concurrent::Tuple.new(size)) do |(item, index), result|
        result.set(index, item)
      end
    end

    def to_set
      ::Set.new(self)
    end

    def to_a💎
      self
    end

    alias_method :to_aDmnd, :to_a💎

    def to_h💎
      h💎[self]
    end

    alias_method :to_hDmnd, :to_h💎

    def to_s💎
      s💎[*self]
    end

    alias_method :to_sDmnd, :to_s💎
  end
end
