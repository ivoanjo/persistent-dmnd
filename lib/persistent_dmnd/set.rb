# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent_dmnd/self_conversion"
require "persistent_dmnd/is_persistent"
require "persistent_dmnd/concurrent_ruby_support"
require "persistent_dmnd/jruby_9_2_set_workaround"

require "immutable"
require "set"

module Persistent💎
  class Set < Immutable::Set
    include SelfConversion
    include IsPersistent
    include Persistent💎

    def self.from_set(set)
      self[*set.to_a]
    end

    def ==(other)
      super || other.respond_to?(:to_set) && set_eq?(other.to_set)
    end

    def is_a?(klass)
      super ||
        # hack to allow Set to be comparable with us
        # and yes a Persistent💎::Set is a Set: go away, stop type-checking and start duck typing!
        klass == ::Set
    end

    def to_set
      ::Set.new(self)
    end

    # Note: Behavior changed from Ruby < 3 (returned 0/nil) to match Ruby 3.0 Set#<=> (returns 0/nil/-1/+1);
    #       see also https://rubyreferences.github.io/rubychanges/3.0.html#standard-library for details
    def <=>(other)
      case size <=> other.size
      when -1
        -1 if self < other
      when +1
        +1 if self > other
      else
        0 if self == other
      end
    end

    def to_a💎
      a💎[*self]
    end

    alias_method :to_aDmnd, :to_a💎

    def to_h💎
      h💎[self]
    end

    alias_method :to_hDmnd, :to_h💎

    def to_s💎
      self
    end

    alias_method :to_sDmnd, :to_s💎

    alias_method :===, :include?

    private

    def set_eq?(other)
      size == other.size && other.all? { |item| include?(item) }
    end
  end
end
