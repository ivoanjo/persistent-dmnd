# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎, and parts were inspired by Ruby's set.rb
# https://github.com/ruby/ruby/blob/v2_6_0_preview2/lib/set.rb
# Copyright (c) 2002-2016 Akinori MUSHA <knu@iDaemons.org>
# All rights reserved.  You can redistribute and/or modify it under the same terms as Ruby. See
# https://github.com/ruby/ruby/blob/v2_6_0_preview2/COPYING for more details.

# frozen_string_literal: true

require "set"

module Persistent💎
  # Workaround for https://github.com/jruby/jruby/issues/5227
  # We monkey patch the Ruby Set methods affected to restore their interoperable behavior
  #
  module JRuby92SetWorkaround
    def self.workaround_needed?
      workaround_checker = Object.new
      workaround_checker.instance_eval do
        def is_a?(klass)
          super || klass == Set
        end

        def size
          0
        end

        def all?
          false
        end
      end

      begin
        Set.new.superset?(workaround_checker)
        false
      rescue ArgumentError => e
        e.message == "value must be a set"
      end
    end

    if RUBY_PLATFORM == "java" && JRUBY_VERSION.start_with?("9.2.", "9.3.", "9.4.") && workaround_needed?
      class ::Set
        # Save existing Set methods

        alias_method :persistent_dmnd_workaround_original_equal, :==
        alias_method :persistent_dmnd_workaround_original_superset?, :superset?
        alias_method :persistent_dmnd_workaround_original_proper_superset?, :proper_superset?
        alias_method :persistent_dmnd_workaround_original_subset?, :subset?
        alias_method :persistent_dmnd_workaround_original_proper_subset?, :proper_subset?
        alias_method :persistent_dmnd_workaround_original_intersect?, :intersect?
        alias_method :persistent_dmnd_workaround_original_disjoint?, :disjoint?
        alias_method :persistent_dmnd_workaround_original_flatten_merge, :flatten_merge
        alias_method :persistent_dmnd_workaround_original_flatten, :flatten
        alias_method :persistent_dmnd_workaround_original_flatten!, :flatten!
        alias_method :persistent_dmnd_workaround_original_spaceship, :<=>

        # Redefine all set methods that use instanceof RubySet to restore previous behavior

        if JRUBY_VERSION.start_with?("9.4.")
          def <=>(other)
            original = persistent_dmnd_workaround_original_spaceship(other)
            return original if original

            return unless other.is_a?(::Set)
            case size <=> other.size
            when -1 then -1 if proper_subset?(other)
            when +1 then +1 if proper_superset?(other)
            else 0 if self.==(other)
            end
          end
        end

        def ==(other)
          persistent_dmnd_workaround_original_equal(other) ||
            (other.is_a?(::Set) && size == other.size && other.all? { |o| include?(o) })
        end

        def superset?(set)
          persistent_dmnd_workaround_original_superset?(set)
        rescue ArgumentError => e
          raise unless e.message == "value must be a set" && set.is_a?(::Set)
          size >= set.size && set.all? { |o| include?(o) }
        end

        def proper_superset?(set)
          persistent_dmnd_workaround_original_proper_superset?(set)
        rescue ArgumentError => e
          raise unless e.message == "value must be a set" && set.is_a?(::Set)
          size > set.size && set.all? { |o| include?(o) }
        end

        def subset?(set)
          persistent_dmnd_workaround_original_subset?(set)
        rescue ArgumentError => e
          raise unless e.message == "value must be a set" && set.is_a?(::Set)
          size <= set.size && all? { |o| set.include?(o) }
        end

        def proper_subset?(set)
          persistent_dmnd_workaround_original_proper_subset?(set)
        rescue ArgumentError => e
          raise unless e.message == "value must be a set" && set.is_a?(::Set)
          size < set.size && all? { |o| set.include?(o) }
        end

        def intersect?(set)
          persistent_dmnd_workaround_original_intersect?(set)
        rescue ArgumentError => e
          raise unless e.message == "value must be a set" && set.is_a?(::Set)
          if size < set.size
            any? { |o| set.include?(o) }
          else
            set.any? { |o| include?(o) }
          end
        end

        def disjoint?(set)
          persistent_dmnd_workaround_original_disjoint?(set)
        rescue ArgumentError => e
          raise unless e.message == "value must be a set" && set.is_a?(::Set)

          !(
            if size < set.size
              any? { |o| set.include?(o) }
            else
              set.any? { |o| include?(o) }
            end
          )
        end

        def flatten_merge(set, seen = ::Set.new)
          set.each do |o|
            if o.is_a?(::Set)
              if seen.include?(o_id = o.object_id)
                raise ArgumentError, "tried to flatten recursive Set"
              end

              seen.add(o_id)
              flatten_merge(o, seen)
              seen.delete(o_id)
            else
              add(o)
            end
          end

          self
        end

        def flatten
          self.class.new.flatten_merge(self)
        end

        def flatten!
          replace(flatten) if any? { |o| o.is_a?(::Set) }
        end

        # Reset aliases to point to redefined methods (otherwise they would keep on pointing to the old variants)

        alias_method :>=, :superset?
        alias_method :>, :proper_superset?
        alias_method :<=, :subset?
        alias_method :<, :proper_subset?
      end
    end
  end
end
