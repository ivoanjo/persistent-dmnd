# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "immutable"

module Persistent💎
  class Dmndifier
    extend Persistent💎

    DEFAULT_OPTIONS = {
      on_unknown: proc { |arg|
        raise ArgumentError, "Could not 💎ify an object of class #{arg.class}. Maybe you need to implement :to_💎?"
      }
    }.freeze

    private_constant :DEFAULT_OPTIONS

    def self.[](arg, options = DEFAULT_OPTIONS)
      options = DEFAULT_OPTIONS.merge(options)
      if arg.respond_to?(:to_💎)
        arg.to_💎
      elsif arg.respond_to?(:to_dmnd)
        arg.to_dmnd
      elsif arg.respond_to?(:to_hash)
        h💎[arg.to_hash]
      elsif arg.is_a?(Immutable::Set) || arg.is_a?(Immutable::SortedSet)
        s💎[*arg.to_a]
      elsif defined?(Hamster::Set) && arg.is_a?(Hamster::Set)
        s💎[*arg.to_a]
      elsif defined?(Hamster::SortedSet) && arg.is_a?(Hamster::SortedSet)
        s💎[*arg.to_a]
      elsif arg.respond_to?(:to_ary)
        a💎[*arg.to_ary]
      elsif defined?(Concurrent::Tuple) && arg.is_a?(Concurrent::Tuple)
        a💎[*arg.to_a]
      elsif arg.respond_to?(:to_set)
        s💎[*arg.to_set.to_a]
      elsif defined?(Concurrent::Map) && arg.is_a?(Concurrent::Map)
        h💎[arg.enum_for(:each_pair).to_h]
      elsif arg.respond_to?(:each_pair)
        h💎[arg.each_pair.to_h]
      else
        options.fetch(:on_unknown).call(arg)
      end
    end
  end
end
