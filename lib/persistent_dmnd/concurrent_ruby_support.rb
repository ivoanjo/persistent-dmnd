# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

module Persistent💎
  # Simple module that attempts to load concurrent-ruby, and then stores the result for quick lookup
  # This way we take care of loading concurrent-ruby ONLY when the library client asks us to, thus not bloating
  # applications that don't need concurrent-ruby loaded but still having a great user experience
  module ConcurrentRubySupport
    REQUIRE_MUTEX = Mutex.new
    private_constant :REQUIRE_MUTEX

    class << self
      def ensure_concurrent_ruby_loaded
        loaded = @concurrent_loaded

        if loaded == :success
          return true
        elsif loaded == :failure
          raise_no_concurrent_ruby
        end

        begin
          REQUIRE_MUTEX.synchronize do # Avoid require races
            require "concurrent"
          end
          mark_as_success
          true
        rescue LoadError
          mark_as_failure
          raise_no_concurrent_ruby
        end
      end

      private

      def raise_no_concurrent_ruby
        raise(NotImplementedError,
          "concurrent-ruby gem is not available, please install it in order to use #to_concurrent and #to_concurrent_* methods")
      end

      # The following methods are only for internal and test usage. Please do not use them :)

      def mark_as_success
        @concurrent_loaded = :success
      end

      def mark_as_failure
        @concurrent_loaded = :failure
      end

      def reset_state
        @concurrent_loaded = nil
      end
    end

    reset_state
  end
end
