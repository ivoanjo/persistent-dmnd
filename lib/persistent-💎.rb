# Persistent-💎: Ruby gem for easily creating immutable data structures
# Copyright (c) 2017-2021 Ivo Anjo <ivo@ivoanjo.me>
#
# This file is part of Persistent-💎.
#
# MIT License
#
# Copyright (c) 2017-2021 Ivo Anjo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# frozen_string_literal: true

require "persistent_dmnd/version"
require "persistent_dmnd/array"
require "persistent_dmnd/hash"
require "persistent_dmnd/set"
require "persistent_dmnd/dmndifier"

module Persistent💎
  def self.included(klass)
    # Make methods on this module also available inside classes (e.g. not just in instances)
    klass.extend(self)
  end

  # Not very useful, but more of a "why not?" thing
  def self.prepended(klass)
    included(klass)
  end

  # Pretty syntax for creating the persistent structures

  # Create a new persistent array
  #
  # @example
  #   my_array = a💎[1, 2, 3]
  #   my_array = a💎.new([1, 2, 3])
  #   # => Persistent💎::Array[1, 2, 3]
  #
  def a💎
    Persistent💎::Array
  end

  alias_method :aDmnd, :a💎

  # Create a new persistent hash
  #
  # @example
  #   my_hash = h💎[hello: 'world']
  #   my_hash = h💎.new(hello: 'world')
  #   # => Persistent💎::Hash[:hello => "world"]
  #
  def h💎
    Persistent💎::Hash
  end

  alias_method :hDmnd, :h💎

  # Create a new persistent set
  #
  # @example
  #   my_set = s💎[:hello, :world]
  #   my_set = s💎.new([:hello, :world])
  #   # => Persistent💎::Set[:hello, :world]
  #
  def s💎
    Persistent💎::Set
  end

  alias_method :sDmnd, :s💎

  # Pretty syntax to making something persistent

  # Make argument persistent, if possible
  #
  # * Already-persistent arguments are left the same
  # * Calls #to_💎 or #to_dmnd for objects that already know how to convert themselves into persistent structures
  # * Converts objects that answer to #to_ary into persistent arrays
  # * Converts objects that answer to #to_hash into persistent hashes
  # * Converts objects that answer to #each_pair into persistent hashes
  # * Converts strings into immutable Strings (without modifying the original String)
  # * Converts objects that answer to #to_str into frozen Strings
  # * Converts sets (e.g. something that #is_a?(::Set) or answers to #to_set) into persistent sets
  # * Converts Hamster (from hamster), Immutable and Concurrent (from concurrent-ruby) data structures to their persistent counterparts
  #
  def 💎ify
    Persistent💎::Dmndifier
  end

  alias_method :dmndify, :💎ify
end

PersistentDmnd = Persistent💎
